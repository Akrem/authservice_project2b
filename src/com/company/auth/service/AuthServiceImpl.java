package com.company.auth.service;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.company.auth.Employee;
import com.company.auth.EmployeeDAO;

@WebService(endpointInterface = "com.company.auth.service.AuthService", serviceName = "corporateAuthService")
public class AuthServiceImpl implements AuthService {

	public Employee getEmployee(String gid) {
		EmployeeDAO dao = new EmployeeDAO();
		return dao.getEmployee(gid);
	}

}